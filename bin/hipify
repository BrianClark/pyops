#!/usr/bin/env python

from pyops import hipchat
import os
import argparse
import sys

parser = argparse.ArgumentParser(
    description='Send HipChat notification via v2 API')
parser.add_argument(
    '-t', '--token', default=os.getenv('HIPCHAT_TOKEN'),
    help="v2 API token  (default: environ['HIPCHAT_TOKEN'])")
parser.add_argument(
    '-r', '--room', default=os.getenv('HIPCHAT_ROOM'),
    help="Room name or API ID to message (default: environ['HIPCHAT_ROOM'])")
parser.add_argument(
    '-f', '--format', choices=['html', 'text'], metavar='HTML | TEXT',
    default=os.getenv('HIPCHAT_FORMAT'),
    help="Notification format (default: environ['HIPCHAT_FORMAT'] else text)")
parser.add_argument(
    '-c', '--color', metavar='COLOR',
    choices=['yellow', 'green', 'red', 'purple', 'gray', 'random'],
    default=os.getenv('HIPCHAT_COLOR'),
    help="Notification color (default: environ['HIPCHAT_COLOR'] else yellow)")
parser.add_argument(
    '-n', '--notify', action='store_true',
    help='trigger a user notification')
parser.add_argument(
    'message', metavar='MESSAGE', nargs='?', help='Notification message')

args = parser.parse_args()

# read from STDIN if present
stdin = None if sys.stdin.isatty() else sys.stdin.read()
if stdin and args.message:
    raise ValueError('Must provide notification message or stdin, not both')

message = stdin if stdin else args.message

if not message:
    raise ValueError('Notification message or stdin required')
if not args.token:
    raise ValueError('HipChat v2 token required')
if not args.room:
    raise ValueError('HipChat room name or ID required')

notify_format = args.format if args.format else 'text'
notify_color = args.color if args.color else 'yellow'

hipchat.hipchat_notify(
    token=args.token,
    room=args.room,
    message=message,
    color=notify_color,
    format=notify_format,
    notify=args.notify)
