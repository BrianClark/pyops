from subprocess import Popen, check_output, PIPE
from contextlib import contextmanager
import logging
import gzip
import os
import tempfile

SQL_DEFAULTS_FILE_CONTENT = """
[client]
user="{0}"
password="{1}"
host="{2}"
port="{3}"
"""


@contextmanager
def defaults_file(username, password, host, port=3306):
    defaults_file = tempfile.NamedTemporaryFile()
    defaults_file_content = SQL_DEFAULTS_FILE_CONTENT.format(
        username, password, host, port)
    defaults_file.write(defaults_file_content)
    defaults_file.flush()
    try:
        yield defaults_file.name
    finally:
        logging.debug(
            "Removing temp mysqldump defaults_file: " + defaults_file.name)
        defaults_file.close()


def get_db_list(defaults_file):
    cmd = ['mysql', '--defaults-file=' + defaults_file,
           '--skip-column-names', '--execute', 'show databases']
    return check_output(cmd).splitlines()


def gzip_dump_database(database, defaults_file, output_dir=None,
                       prefix='', suffix=''):
    output_dir = os.path.getcwd() if output_dir is None else output_dir
    fname = prefix + database + suffix + '.sql.gz'
    path = os.path.join(output_dir, fname)
    try:
        dump_cmd = ['mysqldump', '--defaults-file=' + defaults_file, database]
        logging.debug('gzip_dump_db command: ' + ' '.join(dump_cmd))
        logging.info("Dumping database {0} to file {1}".format(database, path))
        p = Popen(dump_cmd, stdout=PIPE, stderr=PIPE)
        with gzip.open(path, 'wb') as f:
            f.writelines(p.stdout)
        output = p.stderr.read()
        status = p.wait()
        if status:
            raise ValueError(output)
        return path

    except (KeyboardInterrupt, Exception) as e:
        if os.path.exists(path):
            logging.debug('Removing dump file due to exception: ' + path)
            os.remove(path)
        if isinstance(e, OSError):
            message = "Error running command '{0}': {1}".format(
                ' '.join(dump_cmd), str(e.strerror))
            raise RuntimeError(message)
        raise e


def gzip_dump_databases(defaults_file, databases=[], output_dir=None,
                        prefix='', suffix=''):
    databases = databases if databases else get_db_list(defaults_file)
    result = []
    for db in databases:
        if db in ['information_schema', 'innodb', 'performance_schema']:
            continue
        path = gzip_dump_database(
            db, defaults_file, output_dir, prefix, suffix)
        result.append((db, path))
    return result
