import json
import urllib2
import logging


def hipchat_notify(token, room, message, color=None,
                   format=None, notify=None, host=None):
    """Send notification to a HipChat room via API version 2

    Parameters
    ----------
    token : str
        HipChat API version 2 compatible token (room or user token)
    room: str
        Name or API ID of the room to notify
    message: str
        Message to send to room
    color: str, optional
        Background color for message, defaults to yellow
        Valid values: yellow, green, red, purple, gray, random
    format: str, optional
        Format of message, defaults to text
        Valid values: text, html
    notify: bool, optional
        Whether message should trigger a user notification, defaults to False
    host: str, optional
        Host to connect to, defaults to api.hipchat.com
    """

    if len(message) > 10000:
        raise ValueError('Message too long')
    if format not in [None, 'text', 'html']:
        raise ValueError("Invalid message format '{0}'".format(format))
    if color not in [None, 'yellow', 'green', 'red',
                     'purple', 'gray', 'random']:
        raise ValueError("Invalid color `{0}`".format(color))
    if notify not in [None, True, False]:
        raise TypeError("Notify must be boolean or None")

    hc_host = host if host else 'api.hipchat.com'
    hc_notify = False if notify is None else notify
    hc_format = 'text' if format is None else format
    hc_color = 'yellow' if color is None else color

    url = "https://{0}/v2/room/{1}/notification".format(hc_host, room)

    payload = {
        'message': message,
        'notify': hc_notify,
        'message_format': hc_format,
        'color': hc_color
    }

    request = urllib2.Request(url)
    request.add_header('Content-type', 'application/json')
    request.add_header('Authorization', "Bearer " + token)
    request.add_data(json.dumps(payload))
    urllib2.urlopen(request)


class HipChatHandler(logging.Handler):

    def __init__(self, token, room, notify=None, host=None):
        logging.Handler.__init__(self)
        self.token = token
        self.room = room
        self.host = host

    def emit(self, record):
        if hasattr(record, 'color'):
            color = record.color
        else:
            color = self.__color_for_level(record.levelno)

        if hasattr(record, 'room'):
            room = record.room
        else:
            room = self.room

        if hasattr(record, 'notify'):
            notify = record.notify
        elif self.notify is None:
            notify = self.__notify_for_level(record.levelno)
        else:
            notify = self.notify

        hipchat_notify(
            self.token,
            room,
            self.format(record),
            color=color,
            notify=notify,
            host=self.host,
        )

    def __notify_for_level(self, levelno):
        if levelno < logging.WARNING:
            return False
        else:
            return True

    def __color_for_level(self, levelno):
        if levelno > logging.WARNING:
            return 'red'
        if levelno == logging.WARNING:
            return 'yellow'
        if levelno == logging.INFO:
            return 'green'
        if levelno == logging.DEBUG:
            return 'gray'
        return 'purple'
