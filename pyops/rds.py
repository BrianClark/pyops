from boto import rds2
from contextlib import contextmanager
import time
import logging
import mysqldump

MAX_DB_INSTANCE_WAIT = 1800


def describe_db_instance(rds2conn, instance_id):
    resp = rds2conn.describe_db_instances(
        instance_id)['DescribeDBInstancesResponse']
    return resp['DescribeDBInstancesResult']['DBInstances'][0]


def describe_db_snapshot(rds2conn, snapshot_id):
    response = rds2conn.describe_db_snapshots(
        db_snapshot_identifier=snapshot_id)['DescribeDBSnapshotsResponse']
    return response['DescribeDBSnapshotsResult']['DBSnapshots'][0]


def db_instance_exists(rds2conn, instance_id):
    try:
        rds2conn.describe_db_instances(instance_id)
        return True
    except rds2.exceptions.DBInstanceNotFound:
        return False


def db_snapshots(rds2conn, instance_id, snapshot_type=None,
                 marker=None, snaps=[]):

    response = rds2conn.describe_db_snapshots(
        db_instance_identifier=instance_id,
        snapshot_type=snapshot_type,
        marker=marker)['DescribeDBSnapshotsResponse']

    result = response['DescribeDBSnapshotsResult']
    snaps += result['DBSnapshots']
    if result['Marker']:
        db_snapshots(rds2conn, instance_id, snapshot_type,
                     result['Marker'], snaps)
    else:
        snaps[:] = [x for x in snaps if x['Status'] == 'available']
        if snapshot_type:
            snaps[:] = [x for x in snaps if x['SnapshotType'] == snapshot_type]
    return snaps


def latest_db_snapshot(rds2conn, instance_id, snapshot_type=None):
    snaps = db_snapshots(rds2conn, instance_id, snapshot_type)
    if snaps:
        return max(snaps, key=lambda x: x['SnapshotCreateTime'])
    else:
        raise ValueError(
            "No snapshots found for instance '{0}'".format(instance_id))


def restore_from_snapshot(rds2conn, snapshot_id, instance_id, **kwargs):
    logging.info("Restoring snapshot '{0}' as instance '{1}'".format(
        snapshot_id, instance_id))
    resp = rds2conn.restore_db_instance_from_db_snapshot(
        instance_id,
        snapshot_id,
        **kwargs)['RestoreDBInstanceFromDBSnapshotResponse']
    return resp['RestoreDBInstanceFromDBSnapshotResult']['DBInstance']


def modify_db_instance(rds2conn, instance_id, **kwargs):
    if 'apply_immediately' not in kwargs:
        kwargs['apply_immediately'] = True
    logging.info("Modifying RDS instance '{0}'".format(instance_id))
    resp = rds2conn.modify_db_instance(
        instance_id, **kwargs)['ModifyDBInstanceResponse']
    return resp['ModifyDBInstanceResult']['DBInstance']


def delete_db_instance(rds2conn, instance_id, skip_final_snapshot=None,
                       final_db_snapshot_identifier=None):
    logging.info("Deleting RDS instance '{0}'".format(instance_id))
    resp = rds2conn.delete_db_instance(
        instance_id,
        skip_final_snapshot=skip_final_snapshot,
        final_db_snapshot_identifier=final_db_snapshot_identifier
    )['DeleteDBInstanceResponse']
    return resp['DeleteDBInstanceResult']['DBInstance']


def wait_for_db_instance_ready(rds2conn, instance_id, wait_for_pending=True):
    must_end = time.time() + MAX_DB_INSTANCE_WAIT
    while time.time() < must_end:
        instance = describe_db_instance(rds2conn, instance_id)

        status = instance['DBInstanceStatus']
        is_ready = True if status == 'available' else False

        msg = "Waiting for instance {0}... status: {1}".format(
            instance_id, status)

        if wait_for_pending:
            pending = instance['PendingModifiedValues']
            pend_items = [k for k, v in pending.iteritems() if v is not None]
            if pend_items:
                msg = msg + ", pending changes: " + str(pend_items)
                is_ready = False

        vpc_sec_grps = instance['VpcSecurityGroups']
        pend_sgs = [s['VpcSecurityGroupId']
                    for s in vpc_sec_grps if s['Status'] != 'active']
        if pend_sgs:
            msg = msg + ", pending VPC security groups: " + str(pend_sgs)
            is_ready = False

        if is_ready:
            return instance
        else:
            logging.info(msg)
            if status == 'creating':
                time.sleep(30)
            elif status == 'available':
                time.sleep(10)
            else:
                time.sleep(15)

    raise RuntimeError(
        "Exceeded max wait time of {0}s waiting for instance {1} "
        "to become available. Manual intervention may be required.".format(
            MAX_DB_INSTANCE_WAIT, instance_id))


def wait_for_db_instance_delete(rds2conn, instance_id):
    mustend = time.time() + MAX_DB_INSTANCE_WAIT
    while time.time() < mustend:
        try:
            instance = describe_db_instance(rds2conn, instance_id)
        except rds2.exceptions.DBInstanceNotFound:
            return True
        logging.info("Waiting for RDS instance {0} to delete, {1}...".format(
            instance_id, instance['DBInstanceStatus']))
        time.sleep(30)

    raise RuntimeError(
        "Exceeded max wait time of {0}s waiting for instance {1} "
        "to be deleted. Manual intervention may be required.".format(
            MAX_DB_INSTANCE_WAIT, instance_id))


def dump_db_instance(rds2conn, instance_id, password, username=None,
                     output_dir=None, prefix='', suffix='', databases=[]):
    rds = describe_db_instance(rds2conn, instance_id)
    username = rds['MasterUsername'] if username is None else username
    with mysqldump.defaults_file(username=username,
                                 password=password,
                                 host=rds['Endpoint']['Address'],
                                 port=rds['Endpoint']['Port']) as tmpfile:

        return mysqldump.gzip_dump_databases(
            defaults_file=tmpfile,
            databases=databases,
            output_dir=output_dir,
            prefix=prefix,
            suffix=suffix)


@contextmanager
def temp_db_instance_from_snapshot(rds2conn, snapshot_id,
                                   instance_id, **kwargs):
    restore_from_snapshot(rds2conn, snapshot_id, instance_id, **kwargs)
    logging.info("Created temp RDS instance: " + instance_id)
    try:
        temp_rds = wait_for_db_instance_ready(rds2conn, instance_id)
        yield temp_rds
    finally:
        logging.info("Deleting temp RDS instance: " + instance_id)
        delete_db_instance(rds2conn, instance_id, skip_final_snapshot=True)
