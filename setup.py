from distutils.core import setup

setup_config = dict(
    name='pyops',
    version='0.1.0',
    description='Misc Python DevOps Stuff',
    long_description=open('README.md').read(),
    author='Brian Clark',
    author_email='brian@clark.zone',
    packages=['pyops'],
    scripts=['bin/hipify', 'bin/myrdsdump'],
    install_requires=['boto'],
)

setup(**setup_config)
